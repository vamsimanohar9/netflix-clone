import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyB30RcGS2RMt5olmqzI9gShxPGpzkUHeEA",
    authDomain: "netflix-89f63.firebaseapp.com",
    projectId: "netflix-89f63",
    storageBucket: "netflix-89f63.appspot.com",
    messagingSenderId: "995617808906",
    appId: "1:995617808906:web:22123dd39b62e09b7975f3"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth = firebase.auth();

  export {auth}
  export default db;