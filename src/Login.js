import React ,{useState} from 'react'
import './Login.css'
import SignUp from './SignUp';

function Login() {
    const [signIn,setSignIn] = useState(false);
    return (
        <div className='loginScreen'>
        <div className="loginScreen-Background">
                <img className='loginscreen-logo' src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png" alt="" />
                <button onClick={()=> setSignIn(true)} className="signbutton">
                    Sign In
                </button>

                <div className="lognscreen-gradient"></div>
            </div>
       
            <div className='loginscreen-body'>
            {signIn? (
            <SignUp />
        ):(
            
            <>
                    <h1>Unlimited films,TV Programmes and more.</h1>
                    <h2>Watch anywhere. Cancel any time</h2>
                    <h3>Ready to watch? Enter your email to create or restart your membership.</h3>

                    <div className='loginscreen-input'>
                        <form>
                            <input type="email" placeholder='Email Address' />
                            <button onClick={()=> setSignIn(true)} className='getstarted'>Get Started</button>
                        </form>
                    </div>
                </>
                )}
                </div>
        </div>
    )
}

export default Login
