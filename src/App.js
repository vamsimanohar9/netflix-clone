import React,{useEffect} from 'react';
import './App.css';
import Homepage from './Homepage';
import Login from './Login';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';
import { auth } from './firebase';
import {login, logout, selectUser} from './features/counter/userSlice';
import ProfileScreen from './ProfileScreen';

function App() {
  const user = useSelector(selectUser)
  const dispatch = useDispatch();

  useEffect(() =>{
  const unsubscribe = auth.onAuthStateChanged((userAuth) =>{
      if(userAuth) {
        dispatch(
          login({
          uid:userAuth.uid,
          email:userAuth.email,
        }))
      }else{
        dispatch(logout())
      }
    })
    return unsubscribe;
  },[])
  return (
    <div className="App">
      <Router>
      {!user ? (
        <Login />
      ):(
        <Switch>
        <Route path='/profile'>
          <ProfileScreen />
        </Route>
          <Route path='/'>
           <Homepage />
          </Route>
        </Switch>
      )}
        
    </Router>
    </div>
  );
}

export default App;
