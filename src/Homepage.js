import React from 'react'
import './Homepage.css'
import Navbar from './Navbar'
import Banner from './Banner'
import Row from './Row'
import requests from './Request'

function Homepage() {
    return (
        <div className='homepage'>
        <Navbar />
        <Banner />
        <Row title='Trending Now' fetchUrl={requests.fetchTrending}/>
        <Row title='Comedy Movies' fetchUrl={requests.fetchComedyMovies}/>
        <Row title='Documentaries' fetchUrl={requests.fetchDocumentaries}/>
        <Row title='Horror Movies' fetchUrl={requests.fetchHorrorMovies}/>
        <Row title='Netflix Original' fetchUrl={requests.fetchNetflixOriginals}/>
        <Row title='Romance Movies' fetchUrl={requests.fetchRomanceMovies}/>
        </div>
    )
}

export default Homepage
