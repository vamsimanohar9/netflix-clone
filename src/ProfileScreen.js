import React from 'react'
import './ProfileScreen.css'
import Navbar from './Navbar'
import { selectUser } from './features/counter/userSlice'
import { useSelector } from 'react-redux'
import { auth } from './firebase'
import {useHistory} from 'react-router-dom'



function ProfileScreen() {
    const history = useHistory ()
    const user = useSelector(selectUser)
    return (
        <div className='profilescreen'>
            <Navbar />
            <div className="profilescreen_body">
                <h1>Profile</h1>
                <div className="profilescreen_info">
                <img className='profileimage' onClick={()=>history.push('/')} src="https://pbs.twimg.com/profile_images/1240119990411550720/hBEe3tdn_400x400.png" alt="" />
                <div className="profilescreen_details">
                    <h2>{user.email}</h2>
                    <div className="profilescreen_plans">
                        <button onClick={() => auth.signOut()} className='profilescreen_signout'>Sign Out</button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default ProfileScreen
